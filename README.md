## Dotfiles

This repository contains the dotfiles I use on my personal Linux system. The dotfiles include configuration files for window managers and applications; you will also find scripts (mostly in the bin directory) that I use to perform a range of tasks. Whilst this GitLab repository was created so that I can access my dotfiles whenever I install or reinstall a system, you are free to use them under the terms of an MIT Licence. However, you should be aware that these files are often a work in progress so I make no guarantees about how well they will work for you.

## Licence

The content of this repository is licensed under the MIT Licence. Basically, that gives you the right to use, copy, modify, etc. the content how you see fit. You can read the full licence terms here ([https://opensource.org/licenses/MIT](https://opensource.org/licenses/MIT)).
