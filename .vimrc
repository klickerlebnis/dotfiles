" CONFIG syntax
" syntax highlighting for programming languages
syntax on

" CONFIG sound
" no sound effects
set noerrorbells

" CONFIG tabs
" tabstop: 4 chars long
" softtabstop: 4 spaces long
" shiftwidth: 4 spaces shift
set tabstop=4 softtabstop=4
set shiftwidth=4
" expandtab: convert tab char to spaces
set expandtab
" automated indent
set smartindent

" CONFIG lines
" line numbers
set nu
" no line wrapping
set nowrap

" CONFIG search
" case sensitive searching until manual capital letters
set smartcase
" incremental search (while searching getting results)
set incsearch

" CONFIG backups
" no little swap files
set noswapfile
" no backups needed when working with undodir
set nobackup
" undodir: location of undodir (needed dir: undodir)
set undodir=~/.vim/undodir
" undofile: enable file based undos
set undofile

" CONFIG colors
" set column color
set colorcolumn=80
" color highlight settings
highlight ColorColumn ctermbg=0 guibg=lightgrey

" CONFIG Pluggins with vim-plug
" install vim-plug if needed
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

" great color schema
Plug 'morhetz/gruvbox'

" faster grep
Plug 'jremmen/vim-ripgrep'

" needed for git for git blames, git logs, etc.
Plug 'tpope/vim-fugitive'

" TS files nicely colored, better indenting
Plug 'leafgarland/typescript-vim'

" man pages
Plug 'vim-utils/vim-man'

" great for c++
Plug 'lyuts/vim-rtags'

" nice file finding
Plug 'kien/ctrlp.vim'

" autocompletion for programming languages
Plug 'Valloric/YouCompleteMe'

" undotree in a sidebar
Plug 'mbbill/undotree'

call plug#end()

" setting colorscheme
colorscheme gruvbox
set background=dark

" CONFIG rip grep plug
" helps rg to detect root, detect git root, to be aware of git ignore
if executable('rg')
    let g:rg_derive_root='true'
endif

" CONFIG ctrlp
" helps to integrate git features
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']

" file tree splitt
let g:netrw_browse_split = 2

" file tree banner config, 0 for no extra help info
let g:netrw_banner = 0

" window size in percent
let g:netrw_winsize = 25

" ag is fast enough that CtrlP does not need to cache
let g:ctrlp_use_caching = 0


" CONFIG key bindings
" config leader key
let mapleader = " "

" general key bindings for better window switching
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>
nnoremap <leader>u :UndotreeShow<CR>
nnoremap <leader>pv :wincmd v<bar> :Ex <bar> :vertical resize 30<CR>
nnoremap <leader>ps :Rg<SPACE>
nnoremap <silent> <leader>+ :vertical resize +5<CR>
nnoremap <silent> <leader>- :vertical resize -5<CR>
" vnoremap J :m '>+1<CR>gv=gv
" vnoremap K :m '<-2<CR>gv=gv

" YCM key bindings
nnoremap <buffer> <silent> <leader>gd :YcmCompleter GoTo<CR>
nnoremap <buffer> <silent> <leader>gr :YcmCompleter GoToReferences<CR>
nnoremap <buffer> <silent> <leader>rr :YcmCompleter RefactorRename<space>


